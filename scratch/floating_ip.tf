# allocates ip to project
resource "openstack_networking_floatingip_v2" "floating_ip" {
  pool = var.external_network_id
}

# floating ip is associated with vm
resource "openstack_compute_floatingip_associate_v2" "floating_ip_to_basic_instance" {
  floating_ip = openstack_networking_floatingip_v2.floating_ip.address
  instance_id = openstack_compute_instance_v2.basic.id
}
