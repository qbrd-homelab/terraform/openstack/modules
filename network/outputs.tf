output "network" {
  value = openstack_networking_network_v2.network
}

output "subnet" {
  value = openstack_networking_subnet_v2.subnet
}

output "router" {
  value = openstack_networking_router_v2.router
}

output "router_interface" {
  value = openstack_networking_router_interface_v2.router_interface
}
