# Required
variable "external_network_id" {
  type        = string
  description = "ID of the external network to connect to"
}

variable "dns_nameservers" {
  type        = list(string)
  description = "Array of nameservers"
}

variable "name" {
  type        = string
  description = "Name of the internal network"
}

variable "cidr" {
  type        = string
  description = "CIDR of internal network, e.g.: 10.10.10.0/24"
}

# Optional
variable "ip_version" {
  default     = 4
  type        = number
  description = "v4 or v6 designation.  Valid inputs are '4' or '6'"
}
