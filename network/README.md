# OpenStack - Network

Terraform module for creating a Network

## Example

```terraform
module "prod-net" {
  source = "git::https://gitlab.com/qbrd-homelab/terraform/openstack/modules.git//network"
  external_network_name = "public"
  name = "prod"
  cidr = "10.10.10.0/24"
}
```

## Attributes

### Required
* `external_network_name` - Name of the external network to connect
* `name` - Name of the internal network
* `cidr` - CIDR of internal network, e.g.: 10.10.10.0/24

### Optional

* `ip_version` - v4 or v6 designation.  Valid inputs are '4' or '6'

## Outputs

* `external_network` - data.openstack_networking_network_v2.external_network
* `network` - openstack_networking_network_v2.network
* `subnet` - openstack_networking_subnet_v2.subnet
* `router` - openstack_networking_router_v2.router
* `router_interface` - openstack_networking_router_interface_v2.router_interface

