resource "openstack_compute_instance_v2" "basic" {
  count           = var.vm_count
  name            = format("%s-%02d", var.vm_name, count.index + 1)
  image_id        = var.image_id
  flavor_id       = var.flavor_id
  key_pair        = var.key_pair
  security_groups = var.security_groups
  user_data       = var.user_data
  network {
    uuid = var.network_id
  }
}
