output "vms" {
  value = {
    for instance in openstack_compute_instance_v2.basic :
    instance.name => instance
  }
}
