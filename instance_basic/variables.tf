variable "image_id" {
  type        = string
  description = "ID of Image to clone"
}

variable "vm_name" {
  type = string
}

variable "vm_count" {
  type = number
}

variable "flavor_id" {
  type        = string
  description = "ID of flavor for Image"
}

variable "key_pair" {
  type        = string
  description = "Name of Keypair to authenticate"
}

variable "security_groups" {
  type        = list(string)
  description = "List of security groups to apply to instance"
}

variable "user_data" {
  type        = string
  default     = ""
  description = "Post boot configuration"
}

variable "network_id" {
  type        = string
  description = "The ID of network to attach to instance"
}
